package nl.utwente.di.temperatureConverter;

public class Converter {
    public int getCelsiusToFahrenheit(String temperature) {
        int celsius = Integer.parseInt(temperature);
        int fahrenheit = (int) ((celsius * 1.8) + 32);
        return fahrenheit;
    }
}
