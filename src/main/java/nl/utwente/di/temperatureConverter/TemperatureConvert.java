package nl.utwente.di.temperatureConverter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
public class TemperatureConvert extends HttpServlet {
    private Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature Convert";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println(
                "<!DOCTYPE HTML>\n"
                        + "<HTML>\n"
                        + "<HEAD><TITLE>" + title + "</TITLE>"
                        + "<LINK REL=STYLESHEET HREF=\"styles.css\">"
                        + "</HEAD>\n"
                        + "<BODY BGCOLOR=\"#FDF5E6\">\n"
                        + "<H1>" + title + "</H1>\n"
                        + "  <P>Temperature in Celsius: " + request.getParameter("temperature") + "\n"
                        + "  <P>Temperature in Fahrenheit: " + converter.getCelsiusToFahrenheit(request.getParameter("temperature"))
                        + "</BODY></HTML>");
    }

}
